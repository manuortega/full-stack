const express = require('express')
const router = express.Router()

router.use('/api/hotels', require('./hotels.routes'));
router.use('/api/payments', require('./payments.routes'));
router.use('/api/notifications', require('./notifications.routes'));


module.exports = router