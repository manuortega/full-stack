const express = require('express');
const router = express.Router();
let data = require('./data.json')

router.get('/', (req, res) => res.json(data.notifications) );

module.exports = router