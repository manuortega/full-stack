const express = require("express")
const { API_PORT} = require('./utils')

const { app, error404 } = require('./midlewares/app')

app.use(require('./routes/index'))

app.get('/', (req, res)=>{
    res.send(200,{message: 'Bienvenido a Api de gestión de hoteles'})
})

app.use( error404 ); 

app.listen(API_PORT, () => { console.log(`Server is running... in port ${API_PORT}`) });

